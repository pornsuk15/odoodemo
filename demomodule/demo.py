from openerp import fields,models

class Demo(models.Model):
	_name = "demo.demo"

	name = fields.Char(size=32,string="Name")
	status = fields.Char(size=32,string="Status")
	createdate = fields.Char(size=32,string="CreateDate")