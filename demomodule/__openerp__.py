{
    'name': 'Demo module',
    'version': '1.0',
    'author': 'Fariz',
    'complexity': 'normal',
    'description': 'Another new module for tutorial',
    'category': '',
    'depends': [],
    'data': [
    	'views/demo_view.xml'
    ],
    'auto_install': False,
    'installable': True,
    'external_dependencies': {
        'python': [],
    },
}
